#!/bin/bash

docker exec -it market-agila-kafka-1 \
  /kafka/bin/kafka-console-consumer.sh \
  --bootstrap-server localhost:9092 \
  --topic market-agila.public.ticker
