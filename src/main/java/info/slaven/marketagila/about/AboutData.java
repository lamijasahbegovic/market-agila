package info.slaven.marketagila.about;

import lombok.Builder;
import lombok.Data;

import java.net.URL;

@Data
@Builder
public class AboutData {
    String application;
    URL applicationUrl;
    String author;
    URL authorUrl;
}
