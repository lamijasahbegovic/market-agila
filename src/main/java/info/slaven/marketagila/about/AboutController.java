package info.slaven.marketagila.about;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.BiFunction;

@RestController
@RequestMapping("/about")
public class AboutController {

    private final static String APPLICATION = "Market Agila";
    private final static URL APPLICATION_URL = urlFromString("https://gitlab.com/slaven.ostojic/market-agila");
    private final static String AUTHOR = "Slaven Ostojic";
    private final static URL AUTHOR_URL = urlFromString("https://slaven.info");

    @GetMapping("/plain")
    public AboutData about() {
        return AboutData.builder()
                .application(APPLICATION)
                .applicationUrl(APPLICATION_URL)
                .author(AUTHOR)
                .authorUrl(AUTHOR_URL)
                .build();
    }

    @GetMapping("/")
    public String aboutAsHtml() {
        BiFunction<String, URL, String> link = (value, url) -> String.format("<a href=\"%s\" target=\"_blank\">%s</a>", url.toString(), value);
        var data = about();
        var application = link.apply(data.application, data.applicationUrl);
        var author = link.apply(data.author, data.authorUrl);
        var format = "<p>%s, by %s</p>";
        return String.format(format, application, author);
    }

    private static URL urlFromString(String value) {
        try {
            return new URL(value);
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }
}
