package info.slaven.marketagila.kraken;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TickerRepository extends JpaRepository<TickerEntity, Long> {
}
