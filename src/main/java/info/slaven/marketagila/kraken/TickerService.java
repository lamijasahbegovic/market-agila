package info.slaven.marketagila.kraken;

import info.slaven.marketagila.kraken.ws.WebSocketClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
public class TickerService {

    private final TickerRepository tickerRepository;
    private final TickerMapper tickerMapper;
    private final ExecutorService executorService;
    private final WebSocketClient webSocketClient;

    public TickerService(TickerRepository tickerRepository, TickerMapper tickerMapper) {
        this.tickerRepository = tickerRepository;
        this.tickerMapper = tickerMapper;
        this.executorService = Executors.newSingleThreadExecutor();
        this.webSocketClient = new WebSocketClient();
    }

    public void start() {
        System.out.println("Starting WebSocket client... ");
        Consumer<Ticker> handler = ticker -> {
            System.out.println("Ticker received: " + ticker.toString());
            var entity = tickerMapper.toEntity(ticker);
            tickerRepository.save(entity);
        };
        executorService.submit(() -> webSocketClient.connect(handler));
    }

    public void stop() {
        webSocketClient.disconnect();
        executorService.shutdown();
        System.out.println("WebSocket client stopped.");
    }

    public List<Ticker> getAll() {
        return tickerRepository.findAll().stream()
                .map(tickerMapper::fromEntity)
                .collect(Collectors.toList());
    }
}
