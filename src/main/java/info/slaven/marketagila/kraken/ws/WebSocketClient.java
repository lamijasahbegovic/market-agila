package info.slaven.marketagila.kraken.ws;

import info.slaven.marketagila.kraken.Ticker;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.WebSocket;
import java.util.function.Consumer;

public class WebSocketClient {

    private static final String URL = "wss://ws.kraken.com";

    private WebSocket webSocket;

    public void connect(Consumer<Ticker> handler) {
        webSocket = HttpClient
                .newHttpClient()
                .newWebSocketBuilder()
                .buildAsync(URI.create(URL), new WebSocketListener(handler))
                .join();
    }

    public void disconnect() {
        webSocket.sendClose(WebSocket.NORMAL_CLOSURE, "client disconnected").join();
        webSocket.abort();
    }
}
