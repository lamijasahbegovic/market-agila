package info.slaven.marketagila.kraken.ws;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import info.slaven.marketagila.kraken.Ticker;

import java.net.http.WebSocket;
import java.time.LocalDateTime;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;

class WebSocketListener implements WebSocket.Listener {
    // from: https://docs.kraken.com/websockets/#message-ticker

    private final Consumer<Ticker> handler;

    WebSocketListener(Consumer<Ticker> handler) {
        this.handler = handler;
    }

    @Override
    public void onOpen(WebSocket webSocket) {
        System.out.println("Connection established.");
        WebSocket.Listener.super.onOpen(webSocket);
        var data = new JsonObject();
        data.addProperty("event", "subscribe");

        var pair = new JsonArray();
        pair.add("XBT/USD");
        data.add("pair", pair);

        var subscription = new JsonObject();
        subscription.addProperty("name", "ticker");
        data.add("subscription", subscription);

        webSocket.sendText(data.toString(), true);
    }

    @Override
    public CompletionStage<?> onText(WebSocket webSocket, CharSequence data, boolean last) {
        try {
            var response = JsonParser.parseString(data.toString()).getAsJsonArray();
            var result = response.get(1).getAsJsonObject();
            var ask = result.get("a").getAsJsonArray().get(0).getAsString();
            var bid = result.get("b").getAsJsonArray().get(0).getAsString();
            var ticker = new Ticker(LocalDateTime.now(), ask, bid);
            handler.accept(ticker);
        } catch (Exception e) {
            // System.out.println("Unable to process message: " + data);
        }
        return WebSocket.Listener.super.onText(webSocket, data, last);
    }

    @Override
    public void onError(WebSocket webSocket, Throwable error) {
        System.out.println("Error occurred: " + webSocket.toString());
        WebSocket.Listener.super.onError(webSocket, error);
    }

    @Override
    public CompletionStage<?> onClose(WebSocket webSocket, int statusCode, String reason) {
        System.out.println("Connection closed. Status code: " + statusCode + ". Reason: " + reason);
        return WebSocket.Listener.super.onClose(webSocket, statusCode, reason);
    }

}
