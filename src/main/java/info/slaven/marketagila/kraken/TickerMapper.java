package info.slaven.marketagila.kraken;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
class TickerMapper {
    Ticker fromEntity(TickerEntity entity) {
        return new Ticker(entity.getTimestamp(), entity.getAsk().toString(), entity.getBid().toString());
    }

    TickerEntity toEntity(Ticker ticker) {
        var entity = new TickerEntity();
        entity.setTimestamp(ticker.timestamp());
        entity.setAsk(new BigDecimal(ticker.ask()));
        entity.setBid(new BigDecimal(ticker.bid()));
        return entity;
    }
}
