package info.slaven.marketagila.kraken;

import java.time.LocalDateTime;

public record Ticker(LocalDateTime timestamp, String ask, String bid) {
}
