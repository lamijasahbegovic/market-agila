package info.slaven.marketagila.kraken;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ticker")
public class TickerController {

    private final TickerService tickerService;

    public TickerController(TickerService tickerService) {
        this.tickerService = tickerService;
    }

    @PostMapping("/start")
    public void start() {
        tickerService.start();
    }

    @PostMapping("/stop")
    public void stop() {
        tickerService.stop();
    }

    @GetMapping("/")
    public List<Ticker> getAll() {
        return tickerService.getAll();
    }
}
