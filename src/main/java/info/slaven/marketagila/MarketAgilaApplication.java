package info.slaven.marketagila;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketAgilaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketAgilaApplication.class, args);
    }
}
