package info.slaven.marketagila.kraken;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class TickerServiceTest {

    @Mock
    private TickerRepository tickerRepository;

    @Mock
    private TickerMapper tickerMapper;

    @InjectMocks
    private TickerService tickerService;

    @Test
    public void testStart() {
        tickerService.start();

        Mockito.verify(tickerMapper, Mockito.timeout(10_000).atLeast(1)).toEntity(Mockito.any());
        Mockito.verify(tickerRepository, Mockito.timeout(10_000).atLeast(1)).save(Mockito.any());
    }

    @Test
    public void testStop() {
        tickerService.stop();
    }

    @Test
    public void testGetAll() {
        var tickers = tickerService.getAll();
        assertTrue(tickers.isEmpty());

        var ticker1 = tickerEntity(1, 13, 12);
        var ticker2 = tickerEntity(2, 15, 14);
        var expected = List.of(ticker1, ticker2);
        Mockito.when(tickerRepository.findAll()).thenReturn(expected);
        Mockito.when(tickerMapper.fromEntity(ticker1)).thenReturn(ticker(ticker1));
        Mockito.when(tickerMapper.fromEntity(ticker2)).thenReturn(ticker(ticker2));

        tickers = tickerService.getAll();
        assertEquals(2, tickers.size());
        assertEquals("13", tickers.get(0).ask());
        assertEquals("12", tickers.get(0).bid());
        assertEquals("15", tickers.get(1).ask());
        assertEquals("14", tickers.get(1).bid());
    }

    private TickerEntity tickerEntity(long id, int ask, int bid) {
        return new TickerEntity(id, LocalDateTime.now(), new BigDecimal(ask), new BigDecimal(bid));
    }

    private Ticker ticker(TickerEntity entity) {
        return new Ticker(entity.getTimestamp(), entity.getAsk().toString(), entity.getBid().toString());
    }
}
