package info.slaven.marketagila.kraken.ws;

import info.slaven.marketagila.kraken.Ticker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.function.Consumer;

@ExtendWith(MockitoExtension.class)
public class WebSocketClientTest {

    @Mock
    Consumer<Ticker> handler;

    @Test
    public void testWebSocket() {
        WebSocketClient webSocketClient = new WebSocketClient();
        webSocketClient.connect(handler);
        Mockito.verify(handler, Mockito.timeout(30_000).atLeast(2)).accept(Mockito.any());
    }
}
