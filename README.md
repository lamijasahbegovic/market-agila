# Market Agila

<a href="https://gitlab.com/slaven.ostojic/market-agila" target="_blank">
    <img src="market-agila.png" alt="Logo" width="160"/>
</a>

> Kraken exchange watcher :eyes:

- Java 17
- Spring Boot
- WebSocket client
- PostgreSQL
- Flyway
- Kafka Connect
- Debezium
- Docker
- Gradle
- Jib (Google Container Tools)

## Backend

#### Build and run

```shell
./gradlew build
./gradlew bootRun
```

#### Build as a Docker image

```shell
./gradlew jibDockerBuild
```

#### Run with Docker Compose

```shell
docker-compose up database backend
```

## Debezium

In order to listen on change stream from PostgreSQL database, do the following:

- Run the solution with `docker-compose up`
- Configure Debezium: `./configure.sh`
- Listen on changes in database on specific Kafka topic: `./consume.sh`
- (Start listening on Kraken API with POST request to `http://localhost:9000/ticker/start`)
- (Stop listening on Kraken API with POST request to `http://localhost:9000/ticker/stop`)
- Stop the solution: `Ctrl+C` and `docker-compose down`
