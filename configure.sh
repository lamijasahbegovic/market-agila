#!/bin/bash

body=$(cat <<EOF
{
  "name": "market-agila-connector",
  "config": {
     "connector.class": "io.debezium.connector.postgresql.PostgresConnector",
     "tasks.max": "1",
     "plugin.name": "pgoutput",
     "database.hostname": "database",
     "database.port": "5432",
     "database.user": "user",
     "database.password": "pass",
     "database.dbname": "ma_db",
     "topic.prefix": "market-agila",
     "decimal.handling.mode": "string",
     "key.converter": "org.apache.kafka.connect.json.JsonConverter",
     "value.converter": "org.apache.kafka.connect.json.JsonConverter",
     "key.converter.schemas.enable": "false",
     "value.converter.schemas.enable": "false",
     "snapshot.mode": "always"
  }
}
EOF
)

curl -i -X POST \
      -H "Accept:application/json" \
      -H "Content-Type:application/json" \
      localhost:8083/connectors/ \
      --data "$body"
